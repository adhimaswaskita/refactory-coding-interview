package handlers

import (
	"net/http"
	"strconv"

	nrf "refactory-coding-interview/lib/responseformat"
	"github.com/gorilla/mux"
)

//GetOneUser is used to get one User by it's ID
func (h *Handler) GetOneUser(w http.ResponseWriter, r *http.Request) {
	rf := nrf.ResponseFormat{}

	params := mux.Vars(r)
	ID := params["id"]

	intID, _ := strconv.Atoi(ID)
	uintID := uint(intID)

	result, err := h.Service.GetOneUser(uintID)
	if err != nil {
		stringErr := err.Error()
		rf.Response(nrf.ERROR, nil, stringErr, w)
		return
	}

	rf.Response(nrf.SUCCESS, result, nil, w)
	return
}
