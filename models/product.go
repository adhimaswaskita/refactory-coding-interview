package models

import (
	"time"
)

//Product represent Product table
type Product struct {
	ID       		uint       `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	Name          	string       `json:"name"`
	Variant			string		`json:"variant"`
	Merchant_ID		uint		`json:"merchant_id"`
	Price			int			`json:"price"`
	Status			bool		`json:"status"`
	Created_at		time.Time	`json:"created_at"`
	Updated_at		time.Time	`json:"updated_at"`
}