package repositories

import (
	"fmt"
	"strings"

	nconfig "refactory-coding-interview/config"
	nmodel "refactory-coding-interview/models"
	"github.com/jinzhu/gorm"

	//Import postgres from gorm and initialize it
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//IRepository is repo contract
type IRepository interface {
	//User
	GetUser() ([]nmodel.User, error)
	GetOneUser(ID uint) (*[]nmodel.User, error)
	CreateUser(*nmodel.User) (*nmodel.User, error)

	GetProduct() ([]nmodel.Product, error)
	CreateProduct(*nmodel.Product) (*nmodel.Product, error)

	AddProductToCart(userID uint, productID uint) (*nmodel.Cart, error)
}

//Repository implements IRepository
type Repository struct {
	DB *gorm.DB
}

//NewRepository is used to open connection to postgres database by config passes from parameter
func NewRepository(repoConfig *nconfig.Repository) (*Repository, error) {
	server := strings.Split(repoConfig.Server, ":")
	postgresConfig := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=disable",
		server[0], server[1], repoConfig.Username, repoConfig.Name, repoConfig.Password)

	db, err := gorm.Open("postgres", postgresConfig)
	if err != nil {
		return nil, err
	}

	repository := &Repository{
		DB: db,
	}

	return repository, nil
}
