package handlers

import (
	"net/http"

	nrf "refactory-coding-interview/lib/responseformat"
)

//GetUser is used to get all User  data
func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	rf := nrf.ResponseFormat{}

	result, err := h.Service.GetUser()
	if err != nil {
		stringErr := err.Error()
		rf.Response(nrf.ERROR, nil, stringErr, w)
		return
	}

	rf.Response(nrf.SUCCESS, result, nil, w)
	return
}
