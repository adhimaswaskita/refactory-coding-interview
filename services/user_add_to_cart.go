package services

import nmodel "refactory-coding-interview/models"

//AddProductToCart is business logic for create Cart
func (s *Service) AddProductToCart(userID uint, productID uint) (*nmodel.Cart, error) {
	//Send data to repository
	result, err := s.Repository.AddProductToCart(userID, productID)
	if err != nil {
		return nil, err
	}

	return result, nil
}
