package repositories

import nmodel "refactory-coding-interview/models"

//GetOneUser get one User detail from User table
func (r *Repository) GetOneUser(ID uint) (*[]nmodel.User, error) {
	var mUser []nmodel.User
	err := r.DB.Raw(`
	SELECT users.full_name, products.name, products.price 
		FROM users 
		INNER JOIN carts ON carts.user_id = users.id 
		INNER JOIN products ON products.id = carts.product_id
	WHERE users.id = %d`, ID).Scan(&mUser).Error
	if err != nil {
		return nil, err
	}

	return &mUser, nil
}
