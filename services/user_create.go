package services

import nmodel "refactory-coding-interview/models"

//CreateUser is business logic for create User
func (s *Service) CreateUser(User *nmodel.User) (*nmodel.User, error) {
	//Send data to repository
	result, err := s.Repository.CreateUser(User)
	if err != nil {
		return nil, err
	}

	return result, nil
}
