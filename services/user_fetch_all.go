package services

import nmodel "refactory-coding-interview/models"

//GetUser is business logic for get all User
func (s *Service) GetUser() ([]nmodel.User, error) {
	result, err := s.Repository.GetUser()
	if err != nil {
		return nil, err
	}

	return result, nil
}
