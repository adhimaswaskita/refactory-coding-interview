module REFACTORY-CODING-INTERVIEW

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	gopkg.in/yaml.v2 v2.3.0
)
