package services

import nmodel "refactory-coding-interview/models"

//GetOneUser is bussiness logic for get one User from database
func (s *Service) GetOneUser(ID uint) (*[]nmodel.User, error) {
	result, err := s.Repository.GetOneUser(ID)
	if err != nil {
		return nil, err
	}

	return result, nil
}
