package handlers

import (
	"net/http"

	nservice "refactory-coding-interview/services"
)

//IHandler is handler contract
type IHandler interface {
	//User
	CreateUser(w http.ResponseWriter, r *http.Request)
	GetUser(w http.ResponseWriter, r *http.Request)
	GetOneUser(w http.ResponseWriter, r *http.Request)

	GetProduct(w http.ResponseWriter, r *http.Request)
	CreateProduct(w http.ResponseWriter, r *http.Request)

	AddProductToCart(w http.ResponseWriter, r *http.Request)
}

//Handler is http request handler that implements IHandler
type Handler struct {
	Service nservice.IService
}

//NewHandler creates Handler object
func NewHandler(s nservice.IService) IHandler {
	handler := &Handler{
		Service: s,
	}
	return handler
}
