package handlers

import (
	"encoding/json"
	"net/http"

	nrf "refactory-coding-interview/lib/responseformat"
	nmodel "refactory-coding-interview/models"
)

//CreateUser is used to create new User
func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	rf := nrf.ResponseFormat{}
	userParam := &nmodel.User{}

	decoder := json.NewDecoder(r.Body)
	_ = decoder.Decode(userParam)

	result, err := h.Service.CreateUser(userParam)
	if err != nil {
		stringErr := err.Error()
		rf.Response(nrf.ERROR, nil, stringErr, w)
		return
	}

	rf.Response(nrf.SUCCESS, result, nil, w)
}
