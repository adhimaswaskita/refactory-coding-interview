package handlers

import (
	"net/http"

	nrf "refactory-coding-interview/lib/responseformat"
)

//GetProduct is used to get all Product  data
func (h *Handler) GetProduct(w http.ResponseWriter, r *http.Request) {
	rf := nrf.ResponseFormat{}

	result, err := h.Service.GetProduct()
	if err != nil {
		stringErr := err.Error()
		rf.Response(nrf.ERROR, nil, stringErr, w)
		return
	}

	rf.Response(nrf.SUCCESS, result, nil, w)
	return
}
