package repositories

import nmodel "refactory-coding-interview/models"

//GetUser is used to get all User Personal data from database
func (r *Repository) GetUser() (mUser []nmodel.User, err error) {
	r.DB.Find(&mUser)

	return mUser, nil
}