package main

import (
	"fmt"
	"net/http"

	nconfig "refactory-coding-interview/config"
	nhandlers "refactory-coding-interview/handlers"
	nmodels "refactory-coding-interview/models"
	nrepo "refactory-coding-interview/repositories"
	nservices "refactory-coding-interview/services"
	"github.com/gorilla/mux"
)

const (
	//FilePath is config filepath
	FilePath = "config.yaml"
)

//NewRouter creates new mux.Router
func NewRouter(h nhandlers.IHandler) *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/user", h.CreateUser).Methods("POST")
	router.HandleFunc("/user", h.GetUser).Methods("GET")
	router.HandleFunc("/user/{id}", h.GetOneUser).Methods("GET")

	router.HandleFunc("/product", h.GetProduct).Methods("GET")
	router.HandleFunc("/product", h.CreateProduct).Methods("POST")

	router.HandleFunc("/user/{id}", h.AddProductToCart).Methods("POST")
	return router
}

//AutoMigrateDB is database auto migration using gorm
func AutoMigrateDB(repository *nrepo.Repository) {
	repository.DB.AutoMigrate(&nmodels.User{})
	repository.DB.AutoMigrate(&nmodels.Product{})
	repository.DB.AutoMigrate(&nmodels.Cart{})

	repository.DB.Model(&nmodels.Cart{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
	repository.DB.Model(&nmodels.Cart{}).AddForeignKey("product_id", "products(id)", "RESTRICT", "RESTRICT")
}

func main() {

	config, err := nconfig.NewConfig(FilePath)
	if err != nil {
		fmt.Printf("Error read config file : %v", err)
	}

	repository, err := nrepo.NewRepository(config.Repo)
	if err != nil {
		fmt.Printf("Failed connect to database : %v", err)
		return
	}

	AutoMigrateDB(repository)

	service := nservices.NewService(repository)
	handler := nhandlers.NewHandler(service)

	router := NewRouter(handler)

	http.Handle("/", router)

	fmt.Printf("Listening on port%v \n", config.App.Server)
	http.ListenAndServe(config.App.Server, nil)
}
