package services

import nmodel "refactory-coding-interview/models"

//GetProduct is business logic for get all Product
func (s *Service) GetProduct() ([]nmodel.Product, error) {
	result, err := s.Repository.GetProduct()
	if err != nil {
		return nil, err
	}

	return result, nil
}
