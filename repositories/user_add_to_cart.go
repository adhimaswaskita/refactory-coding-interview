package repositories

import nmodel "refactory-coding-interview/models"

//AddProductToCart
func (r *Repository) AddProductToCart(userID, productID uint) (*nmodel.Cart, error) {
	var mCart nmodel.Cart
	err := r.DB.Raw(`
	INSERT INTO carts (user_id, product_id)
	VALUES (%d, %d)`, userID, productID).Scan(&mCart).Error
	if err != nil {
		return nil, err
	}

	return &mCart, nil
}
