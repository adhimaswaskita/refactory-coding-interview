package repositories

import nmodel "refactory-coding-interview/models"

//GetProduct is used to get all Product Personal data from database
func (r *Repository) GetProduct() (mProduct []nmodel.Product, err error) {
	r.DB.Find(&mProduct)

	return mProduct, nil
}