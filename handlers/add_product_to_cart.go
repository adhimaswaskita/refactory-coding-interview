package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	nrf "refactory-coding-interview/lib/responseformat"
	"github.com/gorilla/mux"
)

//AddProductToCart is used to create new Product
func (h *Handler) AddProductToCart(w http.ResponseWriter, r *http.Request) {
	rf := nrf.ResponseFormat{}
	var productId uint

	params := mux.Vars(r)
	userID := params["id"]

	intID, _ := strconv.Atoi(userID)
	uintID := uint(intID)

	decoder := json.NewDecoder(r.Body)
	_ = decoder.Decode(&productId)

	// intProductId, _ := strconv.Atoi(productId)
	// unitProductId := uint(intProductId)

	result, err := h.Service.AddProductToCart(uintID, productId)
	if err != nil {
		stringErr := err.Error()
		rf.Response(nrf.ERROR, nil, stringErr, w)
		return
	}

	rf.Response(nrf.SUCCESS, result, nil, w)
}
