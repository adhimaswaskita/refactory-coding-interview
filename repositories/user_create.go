package repositories

import nmodel "refactory-coding-interview/models"

//CreateUser is used to insert user data to database
func (r *Repository) CreateUser(mUser *nmodel.User) (*nmodel.User, error) {
	err := r.DB.Create(&mUser).Error
	if err != nil {
		return nil, err
	}

	return mUser, nil
}