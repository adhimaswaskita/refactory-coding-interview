package services

import nmodel "refactory-coding-interview/models"

//CreateProduct is business logic for create Product
func (s *Service) CreateProduct(Product *nmodel.Product) (*nmodel.Product, error) {
	//Send data to repository
	result, err := s.Repository.CreateProduct(Product)
	if err != nil {
		return nil, err
	}

	return result, nil
}
