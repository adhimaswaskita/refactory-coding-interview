package services

import (
	nmodel "refactory-coding-interview/models"
	nrepo "refactory-coding-interview/repositories"
)

//IService is service contract
type IService interface {
	//User
	GetUser() ([]nmodel.User, error)
	GetOneUser(ID uint) (*[]nmodel.User, error)
	CreateUser(*nmodel.User) (*nmodel.User, error)

	GetProduct() ([]nmodel.Product, error)
	CreateProduct(*nmodel.Product) (*nmodel.Product, error)

	AddProductToCart(userID uint, productID uint) (*nmodel.Cart, error)
}

//Service is business logic that implements IService
type Service struct {
	Repository nrepo.IRepository
}

//NewService creates service object
func NewService(r nrepo.IRepository) IService {
	service := &Service{
		Repository: r,
	}
	return service
}
