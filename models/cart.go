package models

//Cart represent Cart table
type Cart struct {
	ID		    	   		uint       `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	User_ID 	      		uint       `json:"user_id"`
	Product_ID       		uint       `json:"product_id"`
}