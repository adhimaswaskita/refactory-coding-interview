package models

import (
	"time"

)

//User represent User table
type User struct {
	ID       		uint        `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	Full_Name       string      `json:"full_name"`
	Created_at		time.Time	`json:"created_at"`
	Updated_at		time.Time	`json:"updated_at"`
}